# 🏗️ Custom TR Preview

🛑 **Following this guide may lead to breakage in your save file; you have been warned.**

#### ✔️ Prerequisites 

- [**TD_Graveyard**](https://www.nexusmods.com/morrowind/mods/52000)
  - This should already be included in [Total Overhaul](https://modding-openmw.com/lists/total-overhaul/).
- [**TD_Addon**](https://github.com/TD-Addon/TD_Addon/archive/refs/heads/master.zip)
  - Extract to a new folder called `ModdingResources/TamrielDataAddon`.

#### ⛰️ Landmasses

- Download from the [latest releases](https://www.tamriel-rebuilt.org/releasefiles) or use the following direct links for what I'm using right now:

  - [**Tamriel_Data**](https://drive.google.com/file/d/15TpW-qLlfgOpm502rjBilJXH2BGxIA8h/view)
  - [**TR_Mainland**](https://drive.google.com/file/d/1PG2e-HR1WjE4aVN3MSTwzWIHB6esYcLo/view)
  - [**TR_Factions**](https://drive.google.com/file/d/1oMUyIqOubp8U81B_9MuJJAoustKx4rmZ/view)
  - [**TR_Islands**](https://drive.google.com/file/d/1sqJSRkgtDVkNuidHQP6VaK9rGpj_1HQH/view)
  - [**TR_Narsis**](https://drive.google.com/file/d/15wgvq_ClA4wDTZk2Bik5jvxoAzNX1KbO/view)
  - [**TR_ScopeCreep**](https://drive.google.com/file/d/1PYrpIWw_xAKh_ofve87GqE5yVTKTjgJw/view)
  - [**TR_Othreleth_Woods**](https://drive.google.com/file/d/1k260KOdPGUVG4FBTkUtSdSLh2MwjR76w/view)
  - [**TR_Orethan**](https://github.com/cheflul/chefs-files/blob/main/TR_Orethan%20v2.7z)
  - [**TR_Clambering_Moor**](https://github.com/cheflul/chefs-files/blob/main/TR_Clambering_Moor.7z)

- Extract all to a new folder called `Landmasses/TamrielRebuiltPreview`.


#### 🚧 Preview Warning
- [**Preview Warning for Tamriel Rebuilt**](https://www.nexusmods.com/morrowind/mods/48228) — Download the **TR Preview Warning - Immersive** file.
  - This will provide a warning message telling you when you are entering preview territory.
- Extract to a new folder called `UserInterface/PreviewWarningforTamrielRebuilt`.

#### 📁 Data Paths

    # opemw.cfg
    ...
    data="/home/username/games/OpenMWMods/ModdingResources/TamrielDataGraveyardDeprecationsundeprecated"
    data="/home/username/games/OpenMWMods/ModdingResources/TamrielDataAddon/TD_Addon-master/00 Data Files"
    data="/home/username/games/OpenMWMods/ModdingResources/TamrielDataAddon/TD_Addon-master/01 Data Files - HD Override"
    data="/home/username/games/OpenMWMods/ModdingResources/TamrielDataAddon/TD_Addon-master/02 Data Files - Normal Maps"
    data="/home/username/games/OpenMWMods/Landmasses/TamrielRebuiltPreview"
    ...
    data="/home/username/games/OpenMWMods/UserInterface/PreviewWarningforTamrielRebuilt"
    ...
    
#### 🧩 Plugins

    # openmw.cfg
    ...
    content=TR_Mainland.esm
    content=TR_Narsis.esm
    content=TR_ScopeCreep.esm
    content=TR_Othreleth_Woods.esm
    content=TR_Orethan.esm
    content=TR_Clambering_Moor.esm
    content=TR_Islands_v0009.esp
    ...
    content=TR_Preview_Warning_Immersive.esp
    ...

#### 🏁 Finishing Touches

- Run `delta_plugin`, `groundcoverify.py`, `waza_lightfixes`, and `openmw-navmeshtool` when finished.
- If you use [**omw**](https://gitlab.com/modding-openmw/omw), just run `omw -p YOURPROFILE generate-all` instead.
