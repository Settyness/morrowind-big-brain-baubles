# 📜 Potions & Scrolls and Potion Sorter

This is a combination of two abandoned mods that I have grown used to:

1. [**Potions & Scrolls**](https://web.archive.org/web/20201109025008/mw.modhistory.com/download--2339)
  - Transforms every vanilla potion and scroll icon from the original drab, samely palette to a vibrant, color-coded one with easy-to-recognize symbols.
2. [**Potion Sorter**](https://web.archive.org/web/20161103165608/http://mw.modhistory.com/download--8428)
  - Renames nearly every potion such that they become sorted by effect, rather than quality. Moreover, the qualities are given synonyms so that they are sorted, secondarily, by ascending quality.

Because the former mod was packaged in the esoteric `.ace` format, and contained superfluous edits to chest records, it was [repackaged and combined with the ladder into one mod](https://web.archive.org/web/20221221212139/mw.modhistory.com/download-26-12174). Unfortunately, the only obtainable copy still contains the chest edits.

I have removed the chest edits and patched all the issues I could identify for this package. This includes fixes of previously misaddressed potions and scrolls as well as the addition of brand new icons.

For questions, suggestions, or bugs, [please file an issue on the GitLab tracker](https://gitlab.com/Settyness/morrowind-big-brain-baubles/-/issues).
