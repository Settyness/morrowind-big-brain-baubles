# 🌕️ Vivec Voice Addon Remix

Originally intended to be a simple conversion of a Voices of Vvardenfell unofficial addon into a regular OpenMW mod, this mod improves the source material as well as patches a few outstanding issues.

#### Fixes

- All voice lines have been normalized and are just about as loud as any other line of NPC dialogue.

- `"vivec_god"->Say` is used instead of `PlaySound3D`, which means the dialogue will play on the Voice channel, where it belongs. In turn, this eliminates the need for `StopSound` logic or any optimizations therein.

- File names have been organized and rid of potential spoilers. mwscript has been mostly rewritten to be easier to read and reference.

#### Credits

- [**DuncanWasHere**](https://next.nexusmods.com/profile/DuncanWasHere)
  - [Voices of Vvardenfell Unofficial Addon - Vivec](https://www.nexusmods.com/morrowind/mods/52333)
- [**alvazir**](https://next.nexusmods.com/profile/alvazir) & [**Balketh**](https://next.nexusmods.com/profile/Balketh)
  - [Vivec Voice Addon - Refreshed](https://www.nexusmods.com/morrowind/mods/48955)
- **tomsnellen**
  - [Vivec Voice Addon MW ONLY v1.0](https://www.nexusmods.com/morrowind/mods/594)
  - [Vivec Voice Addon Tribunal Version](https://www.nexusmods.com/morrowind/mods/589)
- [**MOMW Team**](https://modding-openmw.com/about/)
  - Emotional support
