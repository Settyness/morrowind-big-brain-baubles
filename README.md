# 🧠 Big Brain Baubles

A collection of handy-dandy knickknacks made by a Morrowind modding midwit — basically a dumping ground for things I find useful and don't wanna lose. Maybe you'll find them useful as well. 👍️  

# 🔗 Useful Links

A list noteworthy Morrowind links, in addition to the content provided by this repository.

#### ✔️ Prerequisites
> _"I strongly recommend using OpenMW."_
<div style="font-weight: bold;" align="right">—Todd Howard</div>

---

- [**Morrowind**](https://elderscrolls.bethesda.net/en/morrowind) — Buy on [GOG](https://www.gog.com/en/game/the_elder_scrolls_iii_morrowind_goty_edition) or [Steam](https://store.steampowered.com/app/22320/The_Elder_Scrolls_III_Morrowind_Game_of_the_Year_Edition/).
- [**OpenMW**](https://openmw.org) — Open source, modern engine replacement for `Morrowind.exe`.

#### ⚙️ Essential Fixes
> _"It just works."_
<div style="font-weight: bold;" align="right">—Todd Howard</div>

---

- [**Unofficial Morrowind Official Plugins Patched**](https://www.nexusmods.com/morrowind/mods/43931)
  - Patched versions of Bethesda's official plugins.
- [**Patch for Purists**](https://www.nexusmods.com/morrowind/mods/45096)
  - Community patch with optional non-purist fixes.
- [**Expansion Delay**](https://www.nexusmods.com/morrowind/mods/47588)
  -  Prevents early Dark Brotherhood abumishes.
- [**Dubdilla Location Fix**](https://www.nexusmods.com/morrowind/mods/46720)
  - Fixes a major continuity error regarding the location of a dungeon.

#### ❤️ Fargoth's Favorites
> _"Modders accomplish that of which we are incapable."_
<div style="font-weight: bold;" align="right">—Todd Howard</div>

---

- [**NCGDMW Lua Edition**](https://modding-openmw.gitlab.io/ncgdmw-lua/) — **Leveling**
  - Natural and care-free leveling system that is very customizable.
- [**Morrowind Enhanced Textures**](https://www.nexusmods.com/morrowind/mods/46221) — **Textures**
  - Vanilla-friendly upscale of every texture in the game.
- [**Morrowind Optimization Patch**](https://www.nexusmods.com/morrowind/mods/45384) — **Meshes**
  - Greatly improves performance by optimizing many meshes.
- [**Simply Walking (Remastered)**](https://www.nexusmods.com/morrowind/mods/49785) — **Meshes**
  - Improves walking and running animations and with weapon sheathing support.
- [**Jammings Off**](https://www.nexusmods.com/morrowind/mods/44523) — **Meshes**
  - Shrinks bounding boxes of NPCs so they get do not get jammed as frequently.
- [**Chocolate UI**](https://www.nexusmods.com/morrowind/mods/43076) — **User Interface**
  - A sharper, higher quality vanilla-friendly user interface.
- [**Gonzo's Splash Screens**](https://www.nexusmods.com/morrowind/mods/51667) — **User Interface**
  - Very high quality concept art splash screens.
- [**Tamriel Rebuilt**](https://www.tamriel-rebuilt.org/downloads/main-release) — **Landmasses**
  - Allows you to visit the Morrowind mainland. The mod your favorite modders play.
- [**Tamriel Rebuilt Introduction Quest**](https://www.nexusmods.com/morrowind/mods/50445) — **Quests**
  - A quality, early game quest that will guide you to the Tamriel Rebuilt content.
- [**AFFresh**](https://www.nexusmods.com/morrowind/mods/53006) — **Quests**
  - Approximately 30 early game quests, written by a former Morrowind dev.
- [**Fargoth Says Hello**](https://www.nexusmods.com/morrowind/mods/52751) — **Quests**
  - Very important Fargoth character building, written by a former Morrowind dev.

#### 👑 Morrowind's Marvels
> _"Yes, I was in the chess club."_
<div style="font-weight: bold;" align="right">—Todd Howard</div>

---

- [**Starwind**](https://www.nexusmods.com/morrowind/mods/48909) — **Total Conversion**
  - A total conversion Star Wars mod set in The Old Republic universe.
- [**Robowind**](https://www.nexusmods.com/morrowind/mods/53480) — **Total Conversion**
  - More than a mod, this turns OpenMW into an FPS with robots and lasers in space!
- [**Morrowind Rebirth**](https://www.nexusmods.com/morrowind/mods/37795/) — **Total Conversion**
  - The island of Vvardenfell completely reimagined.
- [**Modding-OpenMW.com**](https://modding-openmw.com/) — **And More...**
  - Big brain website and tool project for modlists catered to OpenMW.
