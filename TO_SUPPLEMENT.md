# 🍄 Total Overhaul Supplement

This is a collection of mods that I use in addition to the [**Total Overhaul modlist at Modding-OpenMW.com**](https://modding-openmw.com/lists/total-overhaul). Wherever possible, I link to the mod entry at Modding-OpenMW.com so that their installation patterns can be referenced. The mods are listed roughly in the order in which they are loaded.

> ⚠️ — Unavailable for download due to copyright restrictions.<br />
🛑 — May lead to save breakage; you have been warned.

#### 🩹 PatchesFixesandConsistency

- [OpenMW Distant Ebonheart - Experimental](https://www.nexusmods.com/morrowind/mods/55452)

#### 💪 Animations

- [On the Blink](https://www.nexusmods.com/morrowind/mods/54716)
- [Animated Ectoplasm and Daedra's Heart](https://www.nexusmods.com/morrowind/mods/55386)
- [OpenMW Dynamic Actors](https://www.nexusmods.com/morrowind/mods/54782)
- [Flight Animated for OpenMW](https://www.nexusmods.com/morrowind/mods/54500)

#### 🎥 VFX

- [OpenMW (0.49) Distortion Effects](https://www.nexusmods.com/morrowind/mods/55027)
- [OpenMW Impact Effects](https://www.nexusmods.com/morrowind/mods/55508)

#### 🎧️ Audio

- ⚠️ Nerevar Rising Extended + Reprise
- ⚠️ Oblivion Soundtrack
- ⚠️ Oblivion Quest Update Sound
- ⚠️ Skyrim Soundtrack
- ⚠️ Skyrim Dragonborn - Morrowind Soundtrack Upgrade
- ⚠️ Skyrim Level Up Sound
- [Atmospheric Sound Effects Expanded](https://modding-openmw.com/mods/atmospheric-sound-effects-expanded) — BCOM version
- [Combat Sounds Overhaul](https://modding-openmw.com/mods/combat-sounds-overhaul)
- [Ashlander Voices](https://modding-openmw.com/mods/ashlander-voices)
- [Nordic Banter](https://modding-openmw.com/mods/nordic-banter)
- [Kezymas Voices of Vvardenfell](https://modding-openmw.com/mods/kezymas-voices-of-vvardenfell) — with [OpenMW Voices of Vvardenfell patch](https://modding-openmw.com/mods/openmw-voices-of-vvardenfell-patch)
- [Voices of Vvardenfell Unofficial Addon - Vivec](https://modding-openmw.com/mods/voices-of-vvardenfell-unofficial-addon-vivec)
- [Voices of Vvardenfell Unofficial Addon - TheWisestWomen](https://modding-openmw.com/mods/voices-of-vvardenfell-unofficial-addon-the-wisest-)
- [Voices of Vvardenfell Unofficial Addon - TalkativeTelvanni](https://modding-openmw.com/mods/voices-of-vvardenfell-unofficial-addon-talkative-t)
- [Voices of Vvardenfell Unofficial Addon - YagrumsWisdom](https://modding-openmw.com/mods/voices-of-vvardenfell-unofficial-addon-yagrums-wis)
- [Creeper No Creeping - Unofficial Voices of Vvardenfell Addon](https://www.nexusmods.com/morrowind/mods/53558)
- [Tamriel Rebuilt Voices](https://www.nexusmods.com/morrowind/mods/54454)

#### 🫧 Water

- [OpenMW More Dynamic Water Meshes](https://modding-openmw.com/mods/openmw-more-dynamic-water-meshes/)

#### 🏘️ CitiesTowns

- [Tel Branora Expansion](https://www.nexusmods.com/morrowind/mods/55279)

#### 🪨 CavesandDungeons

- [Dwemer Sky Fortress](https://www.nexusmods.com/morrowind/mods/54747)
- [Cavern Of The Incarnate Overhaul](https://modding-openmw.com/mods/cavern-of-the-incarnate-overhaul/)
- [Dagoth Ur Fleshed Out](https://modding-openmw.com/mods/dagoth-ur-fleshed-out/)

#### 🗣️ NPCs

- [LadyD Reduced Commentary](https://web.archive.org/web/20201112002949/mw.modhistory.com/download-37-12831)
- [Better Merchants Skills for OpenMW](https://modding-openmw.com/mods/better-merchants-skills-for-openmw/)
- [Take Cover (OpenMW)](https://www.nexusmods.com/morrowind/mods/54976)
- [Fair Care (OpenMW)](https://www.nexusmods.com/morrowind/mods/55293)
- [Go Home!](https://modding-openmw.com/mods/go-home/) — remove [Nighttime Door Locks](https://modding-openmw.com/lists/total-overhaul/379/?dev=on)
- [Traveling Guar Riders (OpenMW)](https://www.nexusmods.com/morrowind/mods/52788)

#### 🐴 Creatures

- [Wildlife Attacks NPCs Too - (OpenMW)](https://www.nexusmods.com/morrowind/mods/54448)
- [Creatures Open Doors](https://www.nexusmods.com/morrowind/mods/54567)

#### 🎮️ Gameplay

- RUN
- [Quick Spell Casting](https://www.nexusmods.com/morrowind/mods/52130)
- [Pause Control](https://modding-openmw.com/mods/pause-control/)
- [OpenMW Animated Pickup and Sneak to Steal](https://modding-openmw.com/mods/openmw-animated-pickup-and-sneak-to-steal/)

#### 🪶 Quests

- [Main Quest Enhancers](https://modding-openmw.com/mods/beautiful-cities-of-morrowind) — replaced by [BCOM Waterworks patch](https://www.nexusmods.com/morrowind/mods/49231)
- Some Enchanted Evening
- [Black Soul Gems](https://modding-openmw.com/mods/black-soul-gems/)
- [Ethereal Magic](https://www.nexusmods.com/morrowind/mods/55443)
- [Sea Dog Tavern](https://www.nexusmods.com/morrowind/mods/49978)

#### 🛡️ GuildsFactions

- [Lost Redoran Relics](https://www.nexusmods.com/morrowind/mods/49507)
- [Web of Mephala Morag Tong Headquarters](https://www.nexusmods.com/morrowind/mods/55163)

#### 🎲 Games

- [The Royal Game of Ur](https://www.nexusmods.com/morrowind/mods/44945)
- [Playable Game of Ur](https://www.nexusmods.com/morrowind/mods/53914)
- [Nine-holes](https://www.nexusmods.com/morrowind/mods/52093) — with BCOM + TR modules
- [Thirteen Telvanni - a Dunmeri Card Game](https://www.nexusmods.com/morrowind/mods/52081) — with BCOM + TR modules

#### 🏠️ PlayerHomes

- [Abandoned Flat](https://modding-openmw.com/mods/abandoned-flat-v2) — with included OAAB Brother Junipers Twin Lamps patch
- [Abandoned Flat Containers](https://modding-openmw.com/mods/abandoned-flat-containers)

#### 📕 Books

- [EULA and README books](https://www.nexusmods.com/morrowind/mods/44561)
- [TrueType fonts for OpenMW](https://modding-openmw.com/mods/truetype-fonts-for-openmw/) — HD texture buttons

#### 🪟 UserInterface

- [(OpenMW 0.49) Better Bars](https://www.nexusmods.com/morrowind/mods/54951)
- [(OpenMW) Hit and Miss Percentage Indicators for Combat](https://www.nexusmods.com/morrowind/mods/55396)
- [BuffTimers (For OpenMW)](https://www.nexusmods.com/morrowind/mods/55305)
- [Ultimate Icon Replacer](https://modding-openmw.com/mods/ultimate-icon-replacer)
- [Left Gloves Addon](https://modding-openmw.com/mods/left-gloves-addon/)
- [Vanilla Style HD Icons for Attributes and Skills](https://www.nexusmods.com/morrowind/mods/54708)
- [Potions & Scrolls and Potion Sorter](https://gitlab.com/Settyness/morrowind-big-brain-baubles/-/tree/main/OpenMWMods/UserInterface/PotionsandScrollsandPotionSorter)
- [Skyrim Style Quest Notifications - Legendary Edition](https://modding-openmw.com/mods/skyrim-style-quest-notifications-legendary-edition/)
- [Skyrim Style Quest Notifications Great Patch Hub](https://www.nexusmods.com/morrowind/mods/55480)
- [AvQest Font for OpenMW](https://www.nexusmods.com/morrowind/mods/47042)

#### 💾 Save

- [Friendly Autosave](https://modding-openmw.gitlab.io/friendly-autosave/)
